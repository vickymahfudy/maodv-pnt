# MAODV-PNT

Nama    : Vicky Mahfudy
NRP     : 05111540000105
Kelas   : Jaringan Nirkabel

Ad hoc On-Demand Distance Vector Predicting Node Trend (AODV-PNT) merupakan modifikasi AODV yang menambahkan nilai bobot Total Weight of the Route (TWR) dan menghitung nilai TWR yang akan datang (Future TWR). Kedua nilai tersebut akan mengklasifikasikan node mana saja yang dinilai bagus untuk meneruskan paket RREQ (relay node). Perbedaannya dengan AODV yang asli adalah AODV-PNT hanya melakukan multicast paket RREQ ke Relay Node sedangkan AODV yang asli melakukan broadcast paket RREQ ke semua node tetangga.

Perubahan fungsi:
1) File aodv.h
    - Memberikan comment pada baris link layer detection dan use LL metric di aodv.h untuk mengaktifkan pesan HELLO
    - Menambah atribut lastUpdateTime (menyimpan informasi pada detik ke berapa terjadi perubahan posisi node), lastSpeed (menyimpan informasi kecepatan untuk menghitung akselerasi), dan lastAccel (menyimpan hasil perhitungan akselerasi) pada atribut Class AODV.
    
2) File aodv_packet.h
    - Menambah atribut kecepatan, akselerasi, dan koordinat x dan y pada struct hdr_aodv_reply
    - Menambah array rq_eligible_nodes (menyimpan relay node set) dan nodes_list_len (menyimpan panjang array) pada struct hdr_aodv_request untuk merubah perilaku broadcast RREQ menjadi multicast
    
3) File aodv_rtable.h
    - Menambah atribut kecepatan, akselerasi, dan koordinat neighbor node pada pada class AODV_Neighbor
    
4) File aodv.cc
    - Modifikasi fungsi sendHello untuk pengisian nilai dari atribut paket HELLO
    - Modifikasi fungsi recvHello untuk melakukan pengecekan terhadap neighbor node untuk menambahkan alamat ke dalam list atau memperbarui informasi kecepatan, akselerasi, kordinat dan batas waktu penyimpanan informasi node tersebut di dalam cache
    - Pengolahan TWR, Future TWR dan list Relay Node di dalam fungsi sendRequest dan recvRequest
    - Modifikasi proses pengiriman RREQ pada fungsi sendRequest untuk merubah perilaku broadcast menjadi multicast
    - Modifikasi proses penerimaan RREQ pada fungsi recvRequest untuk menyesuaikan perilaku multicast pada fungsi sendRequest
    
Penambahan file mobilenode.h untuk mendeklarasi class MobileNode yang digunakan untuk mendapatkan informasi-informasi node seperti kecepatan dan kordinat node.
