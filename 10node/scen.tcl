set val(chan) Channel/WirelessChannel ;# channel type
set val(prop) Propagation/TwoRayGround ;# radio-propagation model
set val(netif) Phy/WirelessPhy ;# network interface type
set val(mac) Mac/802_11 ;# MAC type
set val(ifq) Queue/DropTail/PriQueue ;# interface queue type
set val(ll) LL ;# link layer type
set val(ant) Antenna/OmniAntenna ;# antenna model
set val(ifqlen) 50 ;# max packet in ifq
set val(rp) AODV ;# routing protocol
set val(nn) 10 ;
set val(cbrsize) 512 ;# 512 Bytes
set val(cbrrate) 2KB ;# 2kB = 2 kiloBytes
set val(cbrinterval) 1 ;# 1 packet per second
set val(stop) 100
set val(mobilityfile) "coba10node.tcl"
set val(activityfile) "cbr10node.tcl"

# Initialize ns
set ns_ [new Simulator]
set tracefd [open trace.tr w]
$ns_ trace-all $tracefd

# Set up topography object
set topo [new Topography]
$topo load_flatgrid 200 200
set god_ [create-god $val(nn)]
set channel_ [new $val(chan)]
$ns_ node-config -adhocRouting $val(rp) -llType $val(ll) -macType $val(mac) -ifqType $val(ifq) -ifqLen $val(ifqlen) -antType $val(ant) -propType $val(prop) -phyType $val(netif) -channel $channel_ -agentTrace ON -routerTrace ON -macTrace OFF -movementTrace OFF -topoInstance $topo

for {set i 0} {$i < $val(nn)} {incr i} {
set node_($i) [$ns_ node]
$node_($i) random-motion 0
}

puts "Loading mobility file..."
set where [file dirname [info script]]
source [file join $where $val(mobilityfile)]
source [file join $where $val(activityfile)]

# Tell nodes when the simulation ends
for {set i 0} {$i < $val(nn) } {incr i} {
$ns_ at [expr $val(stop) +1.0] "$node_($i) reset";
}

# What to do when scenario finished
$ns_ at [expr $val(stop) +2.0] "puts \"Exiting NS2...\"; $ns_ halt"


puts "Starting simulation..."
$ns_ run
